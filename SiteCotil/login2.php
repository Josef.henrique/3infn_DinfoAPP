
<!DOCTYPE html>
<!-- This site was created in Webflow. http://www.webflow.com-->
<!-- Last Published: Thu May 04 2017 01:20:13 GMT+0000 (UTC) -->
<html data-wf-domain="dinfoapp.webflow.io" data-wf-page="5907683c0493323d42c83eb6" data-wf-site="5907683b0493323d42c83ea9" data-wf-status="1">
<head>
    <meta charset="utf-8">
    <title>
        Login</title>
    <meta content="Contact" property="og:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="https://daks2k3a4ib2z.cloudfront.net/5907683b0493323d42c83ea9/css/dinfoapp.webflow.1a2d709b8.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js">
    </script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]
            }
        });</script>
    <script src="https://daks2k3a4ib2z.cloudfront.net/0globals/modernizr-2.7.1.js" type="text/javascript">
    </script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
<div class="navigation-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
    <div class="w-container">
        <a class="w-nav-brand" href="/">
            <div class="site-name">
                DinfoApp</div>
        </a>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <div class="w-dropdown" data-delay="0">
                <div class="w-dropdown-toggle">
                    <div>
                        Lançamentos</div>
                    <div class="w-icon-dropdown-toggle">
                    </div>
                </div>
                <nav class="w-dropdown-list">
                    <a class="w-dropdown-link" href="#">
                        Médias</a>
                    <a class="w-dropdown-link" href="#">
                        Faltas</a>
                    <a class="w-dropdown-link" href="#">
                        Eventos</a>
                </nav>
            </div>
            <a class="w-nav-link" href="/login">
                Login</a>
        </nav>
        <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu">
            </div>
        </div>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <div class="w-dropdown" data-delay="0">
                <div class="w-dropdown-toggle">
                    <div>
                        Cadastro</div>
                    <div class="w-icon-dropdown-toggle">
                    </div>
                </div>
                <nav class="w-dropdown-list">
                    <a class="w-dropdown-link" href="/departamentos">
                        Departamentos</a>
                    <a class="w-dropdown-link" href="/disciplina">
                        Disciplina</a>
                    <a class="w-dropdown-link" href="#">
                        Pessoas</a>
                    <a class="w-dropdown-link" href="#">
                        Horário</a>
                </nav>
            </div>
        </nav>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <a class="w-nav-link" href="/">
                Home</a>
        </nav>
    </div>
</div>
<div class="content-wrapper">
    <div class="w-container">
        <div class="w-row">
            <div class="w-col w-col-3 w-hidden-small w-hidden-tiny">
                <div class="white-wrapper">
                    <img class="circle-profile" src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907686025dc275d10c3b4a9_cotil_site.png">
                    <p class="site-description">
                        Projeto DinfoApp<br>
                        <br>
                        3INN</p>
                    <div class="grey-rule">
                    </div>
                    <h2 class="small-heading">
                        <em>
                            Desenvolvedores:</em>
                        <br>
                        <br>
                        <em>
                            Josef Henrique</em>
                        <br>
                        <br>
                        <em>
                            Henrique Bastiani</em>
                        <br>
                        <br>
                        <em>
                            Vitor Toledo</em>
                    </h2>
                    <div class="grey-rule">
                    </div>
                    <div class="social-link-group">
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ec5_social-03.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f0c_social-07.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f4f_social-18.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ed2_social-09.svg" width="25">
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-column w-col w-col-9">
                <div class="post-wrapper">
                    <div class="post-content">
                        <div class="post-content">
                            <div class="post-content">
                                <div class="w-form">
                                    <form data-name="Email Form" id="email-form" name="email-form">
                                        <label class="field-label">
                                            <strong class="important-text-2">
                                                Fazer login</strong>
                                        </label>
                                    </form>
                                    <div class="w-form-done">
                                        <div>
                                            Thank you! Your submission has been received!</div>
                                    </div>
                                    <div class="w-form-fail">
                                        <div>
                                            Oops! Something went wrong while submitting the form</div>
                                    </div>
                                </div>
                                <div class="post-content">
                                    <div class="w-form">
                                        <form data-name="Email Form 3" id="email-form-3" name="email-form-3">
                                            <label for="email-2">
                                                Email Address:</label>
                                            <input class="w-input" data-name="Email 2" id="email-2" maxlength="256" name="email" placeholder="Endereço de e-mail:" required="required" type="email">
                                            <label for="email-2">
                                                Senha:</label>
                                            <input class="w-input" data-name="Email 2" id="email-2" maxlength="256" name="email" placeholder="Digite sua senha:" required="required" type="password">
                                            <input class="w-button" data-wait="Please wait..." type="submit" value="Login">
                                        </form>
                                        <div class="w-form-done">
                                            <div>
                                                Thank you! Your submission has been received!</div>
                                        </div>
                                        <div class="w-form-fail">
                                            <div>
                                                Oops! Something went wrong while submitting the form</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-form">
                                <form data-name="Email Form" id="email-form" name="email-form">
                                    <label class="field-label">
                                        <span>
                                            cadastrar<br>
                                        </span>
                                    </label>
                                </form>
                                <div class="w-form-done">
                                    <div>
                                        Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>
                                        Oops! Something went wrong while submitting the form</div>
                                </div>
                            </div>
                            <div class="post-content">
                                <div class="w-form">
                                    <form data-name="Email Form 3" id="email-form-3" name="email-form-3">
                                        <label for="name-2">
                                            RA ou Matrícula:</label>
                                        <input class="w-input" data-name="Name 2" id="name-2" maxlength="256" name="name" placeholder="Informe seu Ra ou Matrícula:" type="text">
                                        <label for="name-2">
                                            Nome Completo:</label>
                                        <input class="w-input" data-name="Name 2" id="name-2" maxlength="256" name="name" placeholder="Digite seu nome completo:" type="text">
                                        <label for="email-2">
                                            Email Address:</label>
                                        <input class="w-input" data-name="Email 2" id="email-2" maxlength="256" name="email" placeholder="Endereço de e-mail:" required="required" type="email">
                                        <label for="email-2">
                                            Senha:</label>
                                        <input class="w-input" data-name="Email 2" id="email-2" maxlength="256" name="email" placeholder="Digite sua senha:" required="required" type="password">
                                        <input class="w-button" data-wait="Please wait..." type="submit" value="Cadastrar">
                                    </form>
                                    <div class="w-form-done">
                                        <div>
                                            Thank you! Your submission has been received!</div>
                                    </div>
                                    <div class="w-form-fail">
                                        <div>
                                            Oops! Something went wrong while submitting the form</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-on-mobile">
                    <div class="white-wrapper">
                        <img class="circle-profile" src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907686025dc275d10c3b4a9_cotil_site.png">
                        <p class="site-description">
                            Projeto DinfoApp<br>
                            <br>
                            3INN</p>
                        <div class="grey-rule">
                        </div>
                        <h2 class="small-heading">
                            <em>
                                Desenvolvedores:</em>
                            <br>
                            <br>
                            <em>
                                Josef Henrique</em>
                            <br>
                            <br>
                            <em>
                                Henrique Bastiani</em>
                            <br>
                            <br>
                            <em>
                                Vitor Toledo</em>
                        </h2>
                        <div class="grey-rule">
                        </div>
                        <div class="social-link-group">
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ec5_social-03.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f0c_social-07.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f4f_social-18.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ed2_social-09.svg" width="25">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-content">
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript">
</script>

<script src="https://daks2k3a4ib2z.cloudfront.net/5907683b0493323d42c83ea9/js/webflow.387774b35.js" type="text/javascript">
</script>

<!--[if lte IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js">
</script>
<![endif]-->

</body>
</html>

