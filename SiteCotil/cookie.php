<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>COOKIE</title>
</head>
<body>

<?php

if(isset($_COOKIE['acessos'])){
    $vezes = $_COOKIE['acessos'] + 1;
}else{
    $vezes = 1;
}

setcookie("acessos", $vezes, time() + 30*24*60*60);

echo "Este é o seu acesso de número ".$vezes."<br>";

?>

</body>
</html>