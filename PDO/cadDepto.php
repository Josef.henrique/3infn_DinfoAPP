<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Departamentos - inclusão</title>
</head>
<body>
<h1>Cadastro de Departamentos</h1>
<?php
require_once "conexao.php";

$acao = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : null;

$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$sigla = isset($_REQUEST['sigla']) ? $_REQUEST['sigla'] : null;
$nome = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : null;
$chefe = isset($_REQUEST['chefe']) ? $_REQUEST['chefe'] : null;

$erro = null;
$deptos = null;
$mensagem=array();

if ($acao == 'incluir') {
    if($sigla!=null && $nome!=null) {

        $sql = "INSERT INTO departamentos ";
        $sql .= "(sigla,nome,chefe)";
        $sql .= "VALUES(?,?,?) ";

        $stmt = $cn->prepare($sql);
        $stmt->bindParam(1, $sigla);
        $stmt->bindParam(2, $nome);
        $stmt->bindParam(3, $chefe);

        if ($stmt->execute()) {
            $erro = $stmt->errorCode();
            $mensagem[$erro]="Departamento criado com sucesso!";
        } else {
            $erro = $stmt->errorCode();
            $mensagem[$erro]=implode(",", $stmt->errorInfo());
        }
    }else{
        $erro = 'Inclusao';
        if($sigla==null){ $mensagem['sigla'] = "Campo SIGLA inválido!";}
        if($nome==null){ $mensagem['nome'] = "Campo NOME inválido!";}
    }
}elseif ($acao == 'excluir') {
    if($id!=null) {
        $sql = "DELETE FROM departamentos ";
        $sql .= "WHERE id=?";
        $stmt = $cn->prepare($sql);
        $stmt->bindParam(1, $id);
        if ($stmt->execute()) {
            $erro = $stmt->errorCode();
            $mensagem['sucesso']="Departamento excluido com sucesso!";
        } else {
            $erro =$stmt->errorCode();
            $mensagem = implode(",", $stmt->errorInfo());
        }
    }else{
        $erro = "Exclusao";
        $mensagem['id'] = "Campo ID ausente!";
    }
}

//Carrega os dados do BD para atualizar a tabela HTML
$sql = "SELECT * FROM departamentos ";
$rs = $cn->prepare($sql);
if($rs->execute()){
    $deptos = $rs->fetchAll(PDO::FETCH_OBJ);
}


?>
<form action="?acao=incluir" method="post">
    Sigla:<br>
    <input type="text"
           name="sigla"
           size="10"
           value="<?php echo $sigla; ?>"
           maxlength="50">
    <BR>
    Nome:<br>
    <input type="text"
           name="nome"
           size="50"
           value="<?php echo $nome; ?>"
           maxlength="50">
    <br>

    Chefe:<br>
    <input type="text"
           name="chefe"
           size="50"
           value="<?php echo $chefe; ?>"
           maxlength="50"> <br>
    <br>
    <input type="submit" name="btnEnviar" value="Enviar">
</form>
<br>

<?php
    if($erro!=null){
        foreach ($mensagem as $msg) {
            echo $msg,"<br>";
        }
    }

    if($deptos!=null){
?>
        <BR>
<table BORDER="1">
    <tr>
        <th>Sigla</th>
        <th>Nome</th>
        <th>Chefe</th>
        <th>Ações</th>
    </tr>
    <?php
        foreach($deptos as $depto)
        {
    ?>
        <tr>
            <td><?php echo $depto->sigla;?></td>
            <td><?php echo $depto->nome;?></td>
            <td><?php echo $depto->chefe;?></td>
            <td>
                <a href="../SiteCotil/editaDepto.php?acao=editar&id=<?php echo $depto->id;?>">
                    Editar</a>
                <a href="cadDepto.php?acao=excluir&id=<?php echo $depto->id;?>">
                    Excluir</a>
            </td>
        </tr>
    <?php
        }
    ?>
</table>
<?php
    }
?>

</body>
</html>

